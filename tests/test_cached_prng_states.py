"""
Test that simulation models and summary tables correctly save and restore
 their PRNG states.
"""

import datetime
import epifx.cmd.forecast as fs
import io
import logging
import numpy as np
import os
import pkgutil
import pypfilt


def test_cached_prng_states(caplog):
    """
    Test that the stochastic SEEIIR model produces identical outputs when the
    same forecast is run twice, the second time starting from a cached state.
    """
    # NOTE: The caplog fixture captures logging; see the pytest documentation:
    # https://docs.pytest.org/en/stable/logging.html
    caplog.set_level(logging.INFO)
    logger = logging.getLogger(__name__)

    toml_file = 'stoch.toml'
    obs_file = 'weekly-cases.ssv'
    pr_file = 'pr-obs.ssv'

    toml_data = pkgutil.get_data('epifx.example.seir', toml_file).decode()
    toml_io = io.StringIO(toml_data)
    instances = list(pypfilt.scenario.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    obs_data = pkgutil.get_data('epifx.example.seir', obs_file).decode()
    with open(obs_file, mode='w') as f:
        f.write(obs_data)

    pr_data = pkgutil.get_data('epifx.example.seir', pr_file).decode()
    with open(pr_file, mode='w') as f:
        f.write(pr_data)

    # NOTE: this needs to be a date for which there is an observation.
    fs_time = datetime.datetime(2014, 4, 6)

    # NOTE: reduce the time this test takes to run by:
    # (a) reducing the number of particles from 1000 to 20; and
    # (b) only forecasting 10 weeks into the future.
    instance.settings['filter']['particles'] = 20
    instance.settings['time']['until'] = fs_time + datetime.timedelta(days=70)

    # NOTE: also record simulated observations for each particle.
    instance.settings['summary']['tables']['sim_obs'] = {
        'component': 'pypfilt.summary.SimulatedObs',
        'observation_unit': 'cases',
    }

    # NOTE: ensure the cache is retained after the first forecast.
    instance.settings['files']['delete_cache_file_before_forecast'] = True
    instance.settings['files']['delete_cache_file_after_forecast'] = False
    context = instance.build_context()

    results_1 = fs.run(context, [fs_time])
    fs_times = list(results_1.keys())
    assert len(fs_times) == 1

    # Check that no cached state was used.
    assert 'No estimation pass needed' not in caplog.text
    # NOTE: this message has *two spaces* so that it aligns with
    # 'Forecasting from' messages.
    assert 'Estimating  from' in caplog.text

    # NOTE: ensure the cache is reused and then removed.
    instance.settings['files']['delete_cache_file_before_forecast'] = False
    instance.settings['files']['delete_cache_file_after_forecast'] = True
    context = instance.build_context()

    results_2 = fs.run(context, [fs_time])
    fs_times = list(results_2.keys())
    assert len(fs_times) == 1

    # Check that a cached state was used.
    assert 'No estimation pass needed' in caplog.text

    # Retrieve the summary tables for each forecast.
    summary_1 = results_1[fs_time].forecasts[fs_time].tables
    summary_2 = results_2[fs_time].forecasts[fs_time].tables

    # Ensure each forecast generated the same collection of tables.
    assert set(summary_1.keys()) == set(summary_2.keys())

    # Ensure that the two forecasts produced identical summary tables.
    for name, table_1 in summary_1.items():
        table_2 = summary_2[name]
        assert table_1.shape == table_2.shape
        # Check that the tables are not empty.
        assert len(table_1) > 0
        assert np.array_equal(table_1.shape, table_2.shape)
        logger.info('Table {} is identical'.format(name))

    # Remove the forecast output file.
    out_file = results_1[fs_time].metadata['forecast_file']
    assert out_file == results_2[fs_time].metadata['forecast_file']
    os.remove(out_file)
