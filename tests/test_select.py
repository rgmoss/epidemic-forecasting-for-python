import io
import logging
import os
import os.path
import numpy as np
import pypfilt
import epifx
import epifx.select


def test_select():
    logging.basicConfig(level=logging.INFO)
    logger = logging.getLogger('epifx.select')
    logger.setLevel(logging.DEBUG)
    out_dir = os.path.abspath('tests')
    out_file = os.path.join(out_dir, 'select_samples.ssv')

    # Define the PRNG seed for the selection process.
    seed = 2020

    # Peak times and sizes based on weekly numbers of seasonal influenza case
    # notifications for metropolitan Melbourne over 2012-2017.
    peak_times = np.array([[136, 177, 176, 182, 187, 193]])
    peak_sizes = np.array([360, 417, 691, 1329, 975, 2036])
    target = epifx.select.TargetPeakMVN(peak_sizes, peak_times)

    toml_io = io.StringIO(config_str())
    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    accepted_fracs = []

    def notify(num_proposed, num_accepted):
        """Record the fraction of proposed particles that were accepted."""
        accepted_fracs.append(num_accepted / num_proposed)

    # Select particles according to the peak size and time target.
    vec = epifx.select.select(instance, target, seed, notify_fn=notify)

    # Check that the acceptance rate was stable.
    accepted_fracs = np.array(accepted_fracs)
    cv = np.std(accepted_fracs) / np.mean(accepted_fracs)
    assert cv < 0.1

    # Retrieve the parameter columns from these particles.
    column_names = ['R0', 'sigma', 'gamma', 'eta', 'alpha', 't0']
    tbl = vec[column_names]

    # Save the sampled parameter values.
    logger.debug('Saving samples to {}'.format(out_file))
    np.savetxt(out_file, tbl, header=' '.join(column_names), comments='')


def config_str():
    """Define forecast scenarios for these test cases."""
    return """
    [components]
    model = "epifx.det.SEIR"
    time = "pypfilt.Datetime"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"

    [time]
    start = "2020-03-01"
    until = "2020-12-31"
    steps_per_unit = 1

    [files]
    input_directory = "."
    output_directory = "."
    temp_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [summary]
    only_forecasts = true
    metadata.packages = [ "epifx" ]

    [filter]
    particles = 2000
    prng_seed = 3001
    history_window = 7
    resample.threshold = 0.25
    regularisation.enabled = true

    [filter.regularisation.bounds]
    R0 = { min = 1.2, max = 1.6 }
    sigma = { min = 0.1, max = 10.0 }
    gamma = { min = 0.1, max = 10.0 }
    eta = { min = 1.0, max = 1.0 }
    alpha = { min = -0.2, max = 0.0 }
    t0 = { min = 0, max = 60 }

    [prior]
    R0 = { name = "uniform", args.loc = 1.2, args.scale = 0.4 }
    sigma = { name = "inverse_uniform", args.low = 0.5, args.high = 4.0 }
    gamma = { name = "inverse_uniform", args.low = 0.5, args.high = 4.0 }
    eta = { name = "constant", args.value = 1.0 }
    # alpha = { name = "constant", args.value = 0.0 }
    t0 = { name = "uniform", args.loc = 0, args.scale = 60 }

    [scenario.Melbourne]
    name = "Melbourne"
    model.population_size = 5_191_000

    [observations.Notifications]
    model = "epifx.obs.PopnCounts"
    observation_period = 7
    file_args.time_col = "date"
    file_args.value_col = "count"
    parameters.bg_obs = 70
    parameters.bg_var = 80
    parameters.pr_obs = 0.00275
    parameters.disp = 100
    descriptor.format.bg_obs = "3.0f"
    descriptor.format.bg_var = "03.0f"
    descriptor.format.pr_obs = "0.5f"
    descriptor.format.disp = "03.0f"
    descriptor.name.bg_obs = "bg"
    descriptor.name.bg_var = "bgvar"
    descriptor.name.pr_obs = "pr"
    descriptor.name.disp = "disp"
    """
