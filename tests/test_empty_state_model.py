"""Test that epifx accepts models with empty state vectors."""

import io
import numpy as np
import pypfilt


def test_empty_state_model():
    toml_io = io.StringIO(config_str())

    instances = list(pypfilt.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    context = instance.build_context()
    start = context.settings['time']['start']
    until = context.settings['time']['until']
    forecast = 10.0
    results = pypfilt.forecast(context, [forecast], filename=None)

    # Sanity check: the model state should remain unchanged.
    init_state = results.forecasts[forecast].history.matrix[0]
    final_state = results.forecasts[forecast].history.matrix[15]
    assert np.array_equal(init_state, final_state)

    # Sanity check: forecast summary tables should be present.
    fs_tables = results.forecasts[forecast].tables
    assert 'sim_obs' in fs_tables
    assert 'forecasts' in fs_tables
    assert 'model_cints' not in fs_tables

    # NOTE: there should be 120 simulated observations; there are 20 particles
    # and 6 observation times.
    sim_obs = fs_tables['sim_obs']
    assert len(sim_obs) == 120
    # All simulated observations should be zero.
    assert np.all(sim_obs['time'] >= forecast)
    assert np.all(sim_obs['time'] <= until)
    assert np.all(sim_obs['value'] == 0.0)

    # NOTE: there should be at least 18 forecast rows; a minimum of 3 CIs and
    # 6 observation times.
    forecasts = fs_tables['forecasts']
    assert len(forecasts) >= 18
    # All forecast CIs should be zero.
    assert np.all(forecasts['time'] >= forecast)
    assert np.all(forecasts['time'] <= until)
    assert np.all(forecasts['ymin'] == 0.0)
    assert np.all(forecasts['ymax'] == 0.0)

    # Sanity check: estimation-pass summary tables should be present.
    est_tables = results.estimation.tables
    assert 'sim_obs' in est_tables
    assert 'forecasts' in est_tables
    assert 'model_cints' not in est_tables

    # NOTE: there should be 220 simulated observations; there are 20 particles
    # and 11 observation times.
    sim_obs = est_tables['sim_obs']
    assert len(sim_obs) == 220
    # All simulated observations should be zero.
    assert np.all(sim_obs['time'] >= start)
    assert np.all(sim_obs['time'] <= forecast)
    assert np.all(sim_obs['value'] == 0.0)

    # NOTE: there should be at least 33 forecast rows; a minimum of 3 CIs and
    # 11 observation times.
    forecasts = est_tables['forecasts']
    assert len(forecasts) >= 33
    # All forecast CIs should be zero.
    assert np.all(forecasts['time'] >= start)
    assert np.all(forecasts['time'] <= forecast)
    assert np.all(forecasts['ymin'] == 0.0)
    assert np.all(forecasts['ymax'] == 0.0)


def config_str():
    """Define forecast scenarios for the NoEpidemic model."""
    return """
    [components]
    model = "epifx.det.NoEpidemic"
    time = "pypfilt.Scalar"
    sampler = "pypfilt.sampler.LatinHypercube"
    summary = "pypfilt.summary.HDF5"

    [time]
    start = 0.0
    until = 15.0
    steps_per_unit = 1

    [files]
    input_directory = "."
    output_directory = "."
    temp_directory = "."
    delete_cache_file_before_forecast = true
    delete_cache_file_after_forecast = true

    [filter]
    particles = 20
    prng_seed = 3001
    history_window = -1
    resample.threshold = 0.25
    regularisation.enabled = true
    results.save_history = true

    [summary]
    only_forecasts = true
    metadata.packages = [ "epifx" ]

    [summary.tables]
    model_cints.component = "pypfilt.summary.ModelCIs"
    sim_obs.component = "pypfilt.summary.SimulatedObs"
    sim_obs.observation_unit = "cases"
    forecasts.component = "pypfilt.summary.PredictiveCIs"

    [scenario.empty_model]
    name = "Empty Model"

    [observations.cases]
    model = "epifx.obs.PopnCounts"
    observation_period = 1
    parameters.bg_obs = 0
    parameters.pr_obs = 1
    parameters.disp = 1000
    descriptor.format = { bg_obs = "3.0f", pr_obs = "0.5f", disp = "03.0f" }
    descriptor.name = { bg_obs = "bg", pr_obs = "pr", disp = "disp" }
    """
