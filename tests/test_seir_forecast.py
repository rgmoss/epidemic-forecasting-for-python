"""Test cases for the SEIR forecasting example."""

import datetime
import epifx.cmd.forecast as fs
import io
import logging
import numpy as np
import os
import os.path
import pkgutil
import pypfilt.io
import pypfilt.scenario


def two_forecast_dates(all_obs, fs_from):
    """Select only two forecasting dates, to reduce computation time."""
    first_obs = min(obs['time'] for obs in all_obs if obs['time'] >= fs_from)
    twelve_weeks_later = first_obs + datetime.timedelta(days=12 * 7)
    return [first_obs, twelve_weeks_later]


def two_forecast_times(all_obs, fs_from):
    """Select only two forecasting times, to reduce computation time."""
    first_obs = min(obs['time'] for obs in all_obs if obs['time'] >= fs_from)
    twelve_weeks_later = first_obs + 12 * 7
    return [first_obs, twelve_weeks_later]


def simulate_seir_observations():
    """Generate synthetic observations from a known model."""
    toml_file = 'seir.toml'
    pr_file = 'pr-obs.ssv'
    out_dir = os.path.abspath('tests')
    obs_file_dt = os.path.join(out_dir, 'simulate_seir_obs_datetime.ssv')
    obs_file_sc = os.path.join(out_dir, 'simulate_seir_obs_scalar.ssv')

    toml_data = pkgutil.get_data('epifx.example.seir', toml_file).decode()
    toml_io = io.StringIO(toml_data)
    instances = list(pypfilt.scenario.load_instances(toml_io))
    assert len(instances) == 1
    instance = instances[0]

    pr_data = pkgutil.get_data('epifx.example.seir', pr_file).decode()
    with open(pr_file, mode='w') as f:
        f.write(pr_data)

    # NOTE: define the fixed ground truth for the model simulation.
    prior = instance.settings['prior']
    prior['R0']['name'] = 'constant'
    prior['R0']['args'] = {'value': 1.45}
    prior['sigma']['name'] = 'constant'
    prior['sigma']['args'] = {'value': 0.25}
    prior['gamma']['name'] = 'constant'
    prior['gamma']['args'] = {'value': 0.25}
    prior['eta']['name'] = 'constant'
    prior['eta']['args'] = {'value': 1.0}
    # prior['alpha']['name'] = 'constant'
    # prior['alpha']['args'] = {'value': 0.0}
    prior['t0']['name'] = 'constant'
    prior['t0']['args'] = {'value': 14.0}

    obs_tables = pypfilt.simulate_from_model(
        instance, particles=1, common_prng_seed=True
    )
    obs_table = obs_tables['cases']

    # Extract weekly observations from the simulated data.
    obs_list = [
        (row['time'], row['value'].astype(int))
        for row in obs_table
        if row['time'].isoweekday() == 7
    ]

    # Save date-indexed observations to disk.
    dt_obs = [(row[0].strftime('%Y-%m-%d'), row[1]) for row in obs_list]
    dt_obs = np.array(dt_obs, dtype=[('time', 'O'), ('value', np.int64)])
    np.savetxt(
        obs_file_dt, dt_obs, fmt='%s %d', header='time cases', comments=''
    )

    # Save day-indexed observations to disk.
    sc_obs = [(int(row[0].strftime('%-j')), row[1]) for row in obs_list]
    sc_obs = np.array(sc_obs, dtype=[('day', np.int64), ('value', np.int64)])
    np.savetxt(
        obs_file_sc, sc_obs, fmt='%d %d', header='day cases', comments=''
    )

    return (obs_list, obs_file_dt, obs_file_sc)


def test_simulate():
    """
    Generate synthetic observations from a known model, and check that the
    serialised results are consistent.
    """
    (obs_list, obs_file_dt, obs_file_sc) = simulate_seir_observations()

    peak_size = max(o[1] for o in obs_list)
    peak_time = [o[0] for o in obs_list if o[1] == peak_size][0]
    assert peak_size == 2678
    assert peak_time.date() == datetime.date(2014, 9, 14)
    peak_day = int(peak_time.strftime('%-j'))

    # Check that the date-indexed peak is consistent with the above results.
    dt_cols = [pypfilt.io.date_column('time'), ('cases', int)]
    dt_obs = pypfilt.io.read_table(obs_file_dt, dt_cols)
    dt_mask = dt_obs['cases'] == peak_size
    assert np.sum(dt_mask) == 1
    assert dt_obs['time'][dt_mask].item() == peak_time

    # Check that the day-indexed peak is consistent with the above results.
    sc_cols = [('day', int), ('cases', int)]
    sc_obs = pypfilt.io.read_table(obs_file_sc, sc_cols)
    sc_mask = sc_obs['cases'] == peak_size
    assert np.sum(sc_mask) == 1
    assert sc_obs['day'][sc_mask] == peak_day

    # Check that the observations are the same.
    assert np.array_equal(sc_obs['cases'], dt_obs['cases'])


def test_seeiir_forecast(caplog):
    """
    Use the SEEIIR forecasting example to compare peak size and time
    predictions at two forecasting dates.

    Note that the observation probability is set to 0.5 (much too high) and so
    we should only obtain sensible forecasts if the observation model is able
    to use the lookup table and obtain observation probabilities from the
    ``pr-obs.ssv`` data file.
    """
    # NOTE: The caplog fixture captures logging; see the pytest documentation:
    # https://docs.pytest.org/en/stable/logging.html
    caplog.set_level(logging.INFO)

    toml_file = 'seeiir.toml'
    obs_file = 'weekly-cases.ssv'
    pr_file = 'pr-obs.ssv'

    toml_data = pkgutil.get_data('epifx.example.seir', toml_file).decode()
    toml_io = io.StringIO(toml_data)

    obs_data = pkgutil.get_data('epifx.example.seir', obs_file).decode()
    with open(obs_file, mode='w') as f:
        f.write(obs_data)

    pr_data = pkgutil.get_data('epifx.example.seir', pr_file).decode()
    with open(pr_file, mode='w') as f:
        f.write(pr_data)

    forecast_from = datetime.datetime(2014, 4, 1)

    # Check that there is only one set of forecasts (i.e., only one location
    # and only one set of observation model parameters).
    instances = pypfilt.scenario.load_instances(toml_io)
    contexts = [inst.build_context() for inst in instances]
    assert len(contexts) == 1
    context = contexts[0]

    # Check that forecasts were run for two forecasting dates.
    forecast_dates = two_forecast_dates(
        context.all_observations, forecast_from
    )
    results = fs.run(context, forecast_dates)
    fs_times = list(results.keys())
    assert len(fs_times) == 2

    fs_time_n1 = fs_times[0]
    fs_time_n2 = fs_times[1]

    # Retrieve the list of observations
    obs = results[fs_time_n1].obs
    peak_size = max(o['value'] for o in obs)
    peak_date = [o['time'] for o in obs if o['value'] == peak_size][0]

    # Check that the peak size and date is as expected.
    assert peak_size == 2678
    assert peak_date == datetime.datetime(2014, 9, 14)

    # Compare the forecast predictions to the observed peak size and date.
    forecast_n1 = results[fs_time_n1].forecasts[fs_time_n1].tables
    forecast_n2 = results[fs_time_n2].forecasts[fs_time_n2].tables

    # Ensure that all of the expected tables have been created, and that no
    # other tables have been created.
    expected_tables = {
        'model_cints',
        'param_covar',
        'pr_epi',
        'forecasts',
        'obs_llhd',
        'peak_size_acc',
        'peak_time_acc',
        'peak_cints',
        'peak_ensemble',
        'exceed_500',
        'exceed_1000',
        'expected_obs',
        'sim_obs',
        'fs_ensemble',
    }
    tables_n1 = set(forecast_n1.keys())
    tables_n2 = set(forecast_n2.keys())
    assert tables_n1 == expected_tables
    assert tables_n2 == expected_tables
    # Ensure that no tables are empty.
    for name in expected_tables:
        shape_n1 = forecast_n1[name].shape
        shape_n2 = forecast_n1[name].shape
        assert len(shape_n1) == 1
        assert len(shape_n2) == 1
        assert shape_n1[0] > 0
        assert shape_n2[0] > 0
    # Ensure that the exceed_500 and exceed_1000 tables differ.
    pr_exc_low_n1 = forecast_n1['exceed_500'][()]['prob']
    pr_exc_high_n1 = forecast_n1['exceed_1000'][()]['prob']
    assert pr_exc_low_n1.shape == pr_exc_high_n1.shape
    assert not np.allclose(pr_exc_low_n1, pr_exc_high_n1)
    # Ensure that the cumulative probability of exceeding 500 cases is greater
    # than that of exceeding 1000 cases, until they both equal 1.0.
    cum_pr_low_n1 = np.cumsum(pr_exc_low_n1)
    cum_pr_high_n1 = np.cumsum(pr_exc_high_n1)
    mask_lt_1 = np.logical_and(cum_pr_low_n1 < 1.0, cum_pr_high_n1 < 1.0)
    mask_gt_0 = np.logical_or(cum_pr_low_n1 > 0.0, cum_pr_high_n1 > 0.0)
    mask = np.logical_and(mask_lt_1, mask_gt_0)
    assert np.all(cum_pr_low_n1[mask] > cum_pr_high_n1[mask])

    # The earlier forecast should include the peak size and time in its 95%
    # credible intervals.
    cints_n1 = forecast_n1['peak_cints']
    ci_n1 = cints_n1[cints_n1['prob'] == 95]
    ci_n1_size_lower = ci_n1['sizemin'].item()
    ci_n1_size_upper = ci_n1['sizemax'].item()
    ci_n1_date_lower = ci_n1['timemin'].item()
    ci_n1_date_upper = ci_n1['timemax'].item()
    assert ci_n1_size_lower <= peak_size <= ci_n1_size_upper
    assert ci_n1_date_lower <= peak_date <= ci_n1_date_upper

    # The later forecast will have narrowed, and it should still include the
    # peak size and time in its 95% credible intervals.
    cints_n2 = forecast_n2['peak_cints']
    ci_n2 = cints_n2[cints_n2['prob'] == 95]
    ci_n2_size_lower = ci_n2['sizemin'].item()
    ci_n2_size_upper = ci_n2['sizemax'].item()
    ci_n2_date_lower = ci_n2['timemin'].item()
    ci_n2_date_upper = ci_n2['timemax'].item()
    assert ci_n2_size_lower <= peak_size <= ci_n2_size_upper
    assert ci_n2_date_lower <= peak_date <= ci_n2_date_upper

    # The later forecast should have more accurate predictions of peak size.
    size_acc_n1 = forecast_n1['peak_size_acc']['acc']
    size_acc_n2 = forecast_n2['peak_size_acc']['acc']
    assert all(size_acc_n1 > 0.3)
    assert any(size_acc_n1 < 0.7)
    assert all(size_acc_n2 > 0.7)

    # The later forecast should have more accurate predictions of peak time.
    time_acc_n1 = forecast_n1['peak_time_acc']['acc']
    time_acc_n2 = forecast_n2['peak_time_acc']['acc']
    assert all(time_acc_n1 > 0.1)
    assert any(time_acc_n1 < 0.3)
    assert all(time_acc_n2 > 0.65)

    # Inspect the forecast ensemble tables.
    ens_n1 = forecast_n1['fs_ensemble'][()]
    ens_n2 = forecast_n2['fs_ensemble'][()]
    # Check that there are more rows for the earlier forecast date.
    assert ens_n1.shape[0] > ens_n2.shape[0]
    # Check that have the expected number of particles.
    num_px = 1000
    mask_ens1 = np.logical_and(
        ens_n1['fs_time'] == ens_n1['fs_time'][0],
        ens_n1['time'] == ens_n1['fs_time'][0],
    )
    mask_ens2 = np.logical_and(
        ens_n2['fs_time'] == ens_n2['fs_time'][0],
        ens_n2['time'] == ens_n2['fs_time'][0],
    )
    assert sum(mask_ens1) == num_px
    assert sum(mask_ens2) == num_px
    # Check that parameter bounds are respected.
    exp_bounds = {
        'R0': (1.35, 1.45),
        'sigma': (0.2, 0.3),
        'gamma': (0.2, 0.3),
        'eta': (1.0, 1.0),
        # 'alpha': (0.0, 0.0),
        't0': (0.0, 28.0),
    }
    for param_name, (min_val, max_val) in exp_bounds.items():
        assert min_val <= np.min(ens_n1[param_name])
        assert max_val >= np.max(ens_n1[param_name])
        assert min_val <= np.min(ens_n2[param_name])
        assert max_val >= np.max(ens_n2[param_name])

    # Clean up: remove created files.
    os.remove(obs_file)
    os.remove(pr_file)
    os.remove(results[fs_time_n1].metadata['forecast_file'])
    os.remove(results[fs_time_n2].metadata['forecast_file'])

    save_tables(context.component['time'], results, 'seeiir_forecast')


def test_seir_forecast(caplog):
    """
    Use the SEIR forecasting example to compare peak size and time predictions
    at two forecasting dates.

    Note that the observation probability is set to 0.5 (much too high) and so
    we should only obtain sensible forecasts if the observation model is able
    to use the lookup table and obtain observation probabilities from the
    ``pr-obs.ssv`` data file.
    """
    # NOTE: The caplog fixture captures logging; see the pytest documentation:
    # https://docs.pytest.org/en/stable/logging.html
    caplog.set_level(logging.INFO)

    toml_file = 'seir.toml'
    obs_file = 'weekly-cases.ssv'
    pr_file = 'pr-obs.ssv'

    toml_data = pkgutil.get_data('epifx.example.seir', toml_file).decode()
    toml_io = io.StringIO(toml_data)

    obs_data = pkgutil.get_data('epifx.example.seir', obs_file).decode()
    with open(obs_file, mode='w') as f:
        f.write(obs_data)

    pr_data = pkgutil.get_data('epifx.example.seir', pr_file).decode()
    with open(pr_file, mode='w') as f:
        f.write(pr_data)

    forecast_from = datetime.datetime(2014, 4, 1)

    # Check that there is only one set of forecasts (i.e., only one location
    # and only one set of observation model parameters).
    instances = pypfilt.scenario.load_instances(toml_io)
    contexts = [inst.build_context() for inst in instances]
    assert len(contexts) == 1
    context = contexts[0]

    # Check that forecasts were run for two forecasting dates.
    forecast_dates = two_forecast_dates(
        context.all_observations, forecast_from
    )
    results = fs.run(context, forecast_dates)
    fs_times = list(results.keys())
    assert len(fs_times) == 2

    fs_time_n1 = fs_times[0]
    fs_time_n2 = fs_times[1]

    # Retrieve the list of observations
    obs = results[fs_time_n1].obs
    peak_size = max(o['value'] for o in obs)
    peak_date = [o['time'] for o in obs if o['value'] == peak_size][0]

    # Check that the peak size and date is as expected.
    assert peak_size == 2678
    assert peak_date == datetime.datetime(2014, 9, 14)

    # Compare the forecast predictions to the observed peak size and date.
    forecast_n1 = results[fs_time_n1].forecasts[fs_time_n1].tables
    forecast_n2 = results[fs_time_n2].forecasts[fs_time_n2].tables

    # The earlier forecast should include the peak size and time in its 95%
    # credible intervals.
    cints_n1 = forecast_n1['peak_cints']
    ci_n1 = cints_n1[cints_n1['prob'] == 95]
    ci_n1_size_lower = ci_n1['sizemin'].item()
    ci_n1_size_upper = ci_n1['sizemax'].item()
    ci_n1_date_lower = ci_n1['timemin'].item()
    ci_n1_date_upper = ci_n1['timemax'].item()
    assert ci_n1_size_lower <= peak_size <= ci_n1_size_upper
    assert ci_n1_date_lower <= peak_date <= ci_n1_date_upper

    # The later forecast will have narrowed, and it should still include the
    # peak size and time in its 95% credible intervals.
    cints_n2 = forecast_n2['peak_cints']
    ci_n2 = cints_n2[cints_n2['prob'] == 95]
    ci_n2_size_lower = ci_n2['sizemin'].item()
    ci_n2_size_upper = ci_n2['sizemax'].item()
    ci_n2_date_lower = ci_n2['timemin'].item()
    ci_n2_date_upper = ci_n2['timemax'].item()
    assert ci_n2_size_lower <= peak_size <= ci_n2_size_upper
    assert ci_n2_date_lower <= peak_date <= ci_n2_date_upper

    # The later forecast should have more accurate predictions of peak size.
    size_acc_n1 = forecast_n1['peak_size_acc']['acc']
    size_acc_n2 = forecast_n2['peak_size_acc']['acc']
    assert all(size_acc_n1 > 0.3)
    assert any(size_acc_n1 < 0.7)
    assert all(size_acc_n2 > 0.7)

    # The later forecast should have more accurate predictions of peak time.
    time_acc_n1 = forecast_n1['peak_time_acc']['acc']
    time_acc_n2 = forecast_n2['peak_time_acc']['acc']
    assert all(time_acc_n1 > 0.1)
    assert any(time_acc_n1 < 0.3)
    assert all(time_acc_n2 > 0.65)

    # Clean up: remove created files.
    os.remove(obs_file)
    os.remove(pr_file)
    os.remove(results[fs_time_n1].metadata['forecast_file'])
    os.remove(results[fs_time_n2].metadata['forecast_file'])

    save_tables(context.component['time'], results, 'seir_forecast')


def test_seeiir_scalar_forecast(caplog):
    """
    Use the SEEIIR forecasting example to compare peak size and time
    predictions at two forecasting dates.

    Note that the observation probability is set to 0.5 (much too high) and so
    we should only obtain sensible forecasts if the observation model is able
    to use the lookup table and obtain observation probabilities from the
    ``pr-obs.ssv`` data file.
    """
    # NOTE: The caplog fixture captures logging; see the pytest documentation:
    # https://docs.pytest.org/en/stable/logging.html
    caplog.set_level(logging.INFO)

    toml_file = 'seeiir_scalar.toml'
    obs_file = 'weekly-cases-scalar.ssv'
    pr_file = 'pr-obs-scalar.ssv'

    toml_data = pkgutil.get_data('epifx.example.seir', toml_file).decode()
    toml_io = io.StringIO(toml_data)

    obs_data = pkgutil.get_data('epifx.example.seir', obs_file).decode()
    with open(obs_file, mode='w') as f:
        f.write(obs_data)

    pr_data = pkgutil.get_data('epifx.example.seir', pr_file).decode()
    with open(pr_file, mode='w') as f:
        f.write(pr_data)

    forecast_from = 91

    instances = list(pypfilt.scenario.load_instances(toml_io))
    assert len(instances) == 1
    contexts = [inst.build_context() for inst in instances]
    assert len(contexts) == 1
    context = contexts[0]

    # Check that forecasts were run for two forecasting dates.
    forecast_dates = two_forecast_times(
        context.all_observations, forecast_from
    )

    results = fs.run(context, forecast_dates)
    fs_times = list(results.keys())
    assert len(fs_times) == 2

    fs_time_n1 = fs_times[0]
    fs_time_n2 = fs_times[1]

    # Retrieve the list of observations
    obs = results[fs_time_n1].obs
    peak_size = max(o['value'] for o in obs)
    peak_date = [o['time'] for o in obs if o['value'] == peak_size][0]

    # Check that the peak size and date is as expected.
    assert peak_size == 2678
    assert peak_date == 257

    # Compare the forecast predictions to the observed peak size and date.
    forecast_n1 = results[fs_time_n1].forecasts[fs_time_n1].tables
    forecast_n2 = results[fs_time_n2].forecasts[fs_time_n2].tables

    # The earlier forecast should include the peak size and time in its 95%
    # credible intervals.
    cints_n1 = forecast_n1['peak_cints']
    ci_n1 = cints_n1[cints_n1['prob'] == 95]
    ci_n1_size_lower = ci_n1['sizemin'].item()
    ci_n1_size_upper = ci_n1['sizemax'].item()
    ci_n1_date_lower = ci_n1['timemin'].item()
    ci_n1_date_upper = ci_n1['timemax'].item()
    assert ci_n1_size_lower <= peak_size <= ci_n1_size_upper
    assert ci_n1_date_lower <= peak_date <= ci_n1_date_upper

    # The later forecast will have narrowed, and it should still include the
    # peak size and time in its 95% credible intervals.
    cints_n2 = forecast_n2['peak_cints']
    ci_n2 = cints_n2[cints_n2['prob'] == 95]
    ci_n2_size_lower = ci_n2['sizemin'].item()
    ci_n2_size_upper = ci_n2['sizemax'].item()
    ci_n2_date_lower = ci_n2['timemin'].item()
    ci_n2_date_upper = ci_n2['timemax'].item()
    assert ci_n2_size_lower <= peak_size <= ci_n2_size_upper
    assert ci_n2_date_lower <= peak_date <= ci_n2_date_upper

    # The later forecast should have more accurate predictions of peak size.
    size_acc_n1 = forecast_n1['peak_size_acc']['acc']
    size_acc_n2 = forecast_n2['peak_size_acc']['acc']
    assert all(size_acc_n1 > 0.3)
    assert any(size_acc_n1 < 0.7)
    assert all(size_acc_n2 > 0.7)

    # The later forecast should have more accurate predictions of peak time.
    time_acc_n1 = forecast_n1['peak_time_acc']['acc']
    time_acc_n2 = forecast_n2['peak_time_acc']['acc']
    assert all(time_acc_n1 > 0.1)
    assert any(time_acc_n1 < 0.3)
    assert all(time_acc_n2 > 0.65)

    # Clean up: remove created files.
    os.remove(obs_file)
    os.remove(pr_file)
    os.remove(results[fs_time_n1].metadata['forecast_file'])
    os.remove(results[fs_time_n2].metadata['forecast_file'])

    save_tables(context.component['time'], results, 'seeiir_scalar_forecast')


def convert_byte_cols(arr):
    """
    Convert byte-string values into string values, for an entire array.
    """
    # NOTE: we assume that 10 Unicode entities are sufficient to store the
    # decoded bytes.
    string_len = 10
    cols = arr.dtype.names
    orig_types = [(col, arr.dtype.fields[col][0].type) for col in cols]
    new_dtype = [
        t if t[1] != np.bytes_ else (t[0], np.str_, string_len)
        for t in orig_types
    ]
    return np.array([convert_row(r) for r in arr], dtype=new_dtype)


def convert_row(row):
    """
    Convert byte-string values into string values, for a single row.
    """

    def datetime_bytes_to_date_str(b):
        if isinstance(b, bytes):
            return b.decode()[:10]
        return b

    return tuple(map(datetime_bytes_to_date_str, row))


def get_col_fmt(t):
    """
    Determine the format specifier for a column type.
    """
    # NOTE: see the hierarchy of NumPy type objects as per the NumPy docs:
    # https://numpy.org/doc/stable/reference/arrays.scalars.html
    fmt_tbl = {
        np.character: '%s',
        np.floating: '%0.6f',
        np.integer: '%d',
    }

    for super_type, fmt in fmt_tbl.items():
        if np.issubdtype(t, super_type):
            return fmt

    raise ValueError('No format defined for {}'.format(t))


def row_format_string(arr):
    """
    Determine the format string for a row from the given array, by inspecting
    the array's structured dtype.
    """
    col_types = [arr.dtype.fields[col][0].type for col in arr.dtype.names]
    col_fmts = [get_col_fmt(t) for t in col_types]
    return ' '.join(col_fmts)


def save_tables(time_scale, results, prefix, save_dir='tests', sep='_'):
    """
    Save rows from tables of interest to disk, so that they can be tracked by
    git, and their contents can be used as regression tests.
    """
    # NOTE: epifx.cmd.forecast.run() runs a separate forecast for each date,
    # and returns a separate 'state' object for each forecast.
    # So we need to index 'state' by the forecast date once, to obtain the
    # state object for that forecast, and by the forecast date again to obtain
    # the results for the forecasting pass.

    # Ensure that there is at least one set of forecasts.
    fs_times = list(results.keys())
    assert len(fs_times) > 0

    # Ensure that the same tables were recorded for each forecast.
    fs_table_names = [
        set(results[fs_time].forecasts[fs_time].tables.keys())
        for fs_time in fs_times
    ]
    assert len(fs_table_names) > 0
    table_names = fs_table_names[0]
    assert all(table_names == names for names in fs_table_names[1:])

    # Each entry in out_map is a tuple that defines:
    # - the output file suffix;
    # - the columns to retain; and
    # - the rows to retain.
    #
    # To save a subset of the table rows, use:
    # - slice(None) to save all rows;
    # - slice(n) to retain the first n rows;
    # - slice(l, u) to retain rows from l (inclusive) to u (exclusive);
    # - slice(None, None, n) to retain every nth row.
    out_map = {
        'exceed_1000': (
            'exceed_1000',
            ['fs_time', 'week_start', 'prob'],
            slice(None),
        ),
        'exceed_500': (
            'exceed_500',
            ['fs_time', 'week_start', 'prob'],
            slice(None),
        ),
        'forecasts': (
            'fs',
            ['fs_time', 'time', 'prob', 'ymin', 'ymax'],
            slice(None),
        ),
        'obs_llhd': (
            'obs_llhd',
            ['fs_time', 'time', 'value', 'llhd'],
            slice(None),
        ),
        'peak_cints': (
            'peak_cints',
            ['fs_time', 'prob', 'sizemin', 'sizemax', 'timemin', 'timemax'],
            slice(None),
        ),
        'peak_size_acc': (
            'peak_size_acc',
            ['fs_time', 'toln', 'acc', 'var', 'avg'],
            slice(None),
        ),
        'peak_time_acc': (
            'peak_time_acc',
            ['fs_time', 'toln', 'acc', 'var', 'avg'],
            slice(None),
        ),
        'pr_epi': ('pr_outbreak', ['time', 'pr'], slice(None)),
    }
    out_ext = '.ssv'

    # NOTE: metadata may not be propagated consistently across NumPy versions.
    #
    # "Although used in certain projects, this feature was long undocumented
    # and is not well supported. Some aspects of metadata propagation are
    # expected to change in the future."
    #
    # https://numpy.org/doc/1.23/reference/generated/numpy.dtype.metadata.html
    first_tables = results[fs_times[0]].forecasts[fs_times[0]].tables
    for table_name in table_names:
        if table_name not in out_map:
            continue
        (suffix, columns, ixs) = out_map[table_name]
        tbl = np.concatenate(
            tuple(
                results[fs_time].forecasts[fs_time].tables[table_name]
                for fs_time in fs_times
            )
        )
        # Retrieve dtype metadata (if any) from the first table.
        first_tbl = first_tables[table_name]
        if first_tbl.dtype.metadata is None:
            time_columns = []
        else:
            time_columns = first_tbl.dtype.metadata.get('time_columns', [])
        tbl = tbl[columns]
        tbl = tbl[ixs]
        tbl = pypfilt.io.ensure_stored_time(time_scale, tbl, time_columns)
        tbl = convert_byte_cols(tbl)
        fmt = row_format_string(tbl)
        out_file = prefix + sep + suffix + out_ext
        out_path = os.path.join(save_dir, out_file)
        np.savetxt(
            out_path,
            tbl,
            fmt=fmt,
            comments='',
            header=' '.join(tbl.dtype.names),
        )
