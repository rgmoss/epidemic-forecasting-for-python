Commands
========

The following commands are provided by `epifx`_:

- :literal:`epifx-forecast` generates :ref:`forecasts <forecast>` from live
  data.

.. _forecast:

Live forecasting
----------------

To generate forecasts from live data, use :literal:`epifx-forecast`.
In the example below, forecasts will be generated for each scenario defined in the file ``scenarios.toml``:

.. code-block:: shell

   epifx-forecast scenarios.toml

You can generate separate forecasts for multiple dates by specifying a date range.
In the example below, separate forecasts will be generated for each date in May 2022 where there is an observation:

.. code-block:: shell

   epifx-forecast --from 2022-05-01 --until 2022-05-31 scenarios.toml

You can distribute forecast simulations across multiple processes by specifying how many processes to use.
In the example below, up to 4 simulations will be run in parallel:

.. code-block:: shell

   epifx-forecast --spawn 4 scenarios.toml

You can run forecasts for a subset of the scenarios defined in a scenario file.
In the example below, forecasts will only be generated for the "Foo" and "Bar" scenarios:

.. code-block:: shell

   epifx-forecast --scenario Foo --scenario Bar scenarios.toml

For further information about ``epifx-forecast`` command-line arguments, see the help output:

.. code-block:: shell

   epifx-forecast --help
