.. _examples:

Example scenarios
=================

The :mod:`epifx.example.seir` module provides example scenarios for each of the provided :ref:`epimodels`, using `Google Flu Trends`_ data for Australia in 2014.

Use the :func:`~epifx.example.seir.write_example_files` function to write the scenario definitions and input data files for a specific scenario to the current working directory.
You can then run forecast simulations with the :ref:`epifx-forecast command <forecast>`.
You can also modify the scenario definitions and input data files to see how your changes affect the forecast results.

.. autofunction:: epifx.example.seir.write_example_files

Shown below is an example script that generates forecasts for the ``'seir'`` scenario:

.. code-block:: python

   import datetime
   import epifx.example.seir
   import pypfilt

   scenario = 'seir'
   toml_file = 'seir.toml'
   output_file = 'seir_forecasts.hdf5'
   forecast_dates = [datetime.datetime(2014, 4, 1)]

   # Write the scenario files to the working directory.
   epifx.example.seir.write_example_files(scenario)

   # Run forecasts for each scenario instance (there is only one instance).
   for instance in pypfilt.load_instances(toml_file):
       context = instance.build_context()
       pypfilt.forecast(context, forecast_dates, output_file)

.. _Google Flu Trends: https://www.google.com/publicdata/explore?ds=z3bsqef7ki44ac_
