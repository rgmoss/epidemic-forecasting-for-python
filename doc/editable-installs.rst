Editable installs
=================

Editable installs are supported with `setuptools >= 40.9.0 <https://setuptools.pypa.io/en/latest/history.html#v40-9-0>`__ and `pip >= 21.1 <https://pip.pypa.io/en/latest/news/#v21-1>`__.
Clone the epifx_ repository and install into a virtual environment:

.. code-block:: shell

   git clone https://bitbucket.org/robmoss/epidemic-forecasting-for-python.git
   virtualenv epifx_venv
   . ./epifx_venv/bin/activate
   pip install -e epidemic-forecasting-for-python/
