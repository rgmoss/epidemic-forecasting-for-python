Testing with nox
================

The epifx_ testing suite uses the `pytest <http://docs.pytest.org/>`__
framework and the `nox <https://nox.thea.codes/>`__ automation tool.
The test cases are contained in the ``./tests`` directory, and can be run with:

.. code-block:: shell

   nox

The ``noxfile.py`` contents are shown below, and include targets that check whether the documentation in ``./doc`` builds correctly.

.. literalinclude:: ../noxfile.py
   :language: python
